package com.example.examenrecetas

import androidx.lifecycle.ViewModel
import com.example.examenrecetas.model.Details
import com.example.examenrecetas.model.Results
import com.example.myapplication.core.network.Repository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class RecipesViewModel: ViewModel() {

    private val _response = MutableStateFlow(Results.empty)
    private val _detail = MutableStateFlow(Details.empty)
    val respose: StateFlow<Results> get(){return _response.asStateFlow()}
    val detail: StateFlow<Details> get(){ return _detail.asStateFlow()}

    fun getRecipeList(){
        CoroutineScope(Dispatchers.IO).launch {
            Repository.getRecipes().let {
                _response.value = it
            }
        }
    }

    fun getDetails(id: String) {
        _detail.value = Details.empty
        CoroutineScope(Dispatchers.IO).launch {
            Repository.getIdDetail(id).let {
                _detail.value = it
            }
        }
    }


}