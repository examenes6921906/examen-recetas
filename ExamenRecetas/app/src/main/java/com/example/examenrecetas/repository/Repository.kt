package com.example.myapplication.core.network

import android.util.JsonReader
import android.util.Log
import com.example.examenrecetas.repository.AppMockup
import com.example.examenrecetas.model.Details
import com.example.examenrecetas.model.Recipe
import com.example.examenrecetas.model.Results
import java.io.BufferedInputStream
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

class Repository {

    companion object {

        private const val TAG = "NETWORK_CALL_SUCCESS"
        private const val ERROR_TAG = "NETWORK_CALL_ERROR"

        suspend fun getRecipes(): Results {
            try {
                val url = URL(AppMockup.BASE_URL + AppMockup.RECIPES)
                val urlConnection: HttpURLConnection = url.openConnection() as HttpURLConnection
                try {
                    val input: InputStream = BufferedInputStream(urlConnection.inputStream)
                    Log.d("log", TAG)
                    val reader = JsonReader(InputStreamReader(input, "UTF-8"))
                    return try {
                        reader.getResults()
                    } catch (io: IOException) {
                        io.printStackTrace()
                        Results.empty
                    }
                } catch (e: IOException) {
                    Log.e("log", ERROR_TAG)
                    return Results.empty
                } finally {
                    urlConnection.disconnect()
                }
            } catch (e: MalformedURLException) {
                e.printStackTrace()
                return Results.empty
            } catch (e: IOException) {
                e.printStackTrace()
                return Results.empty
            }
        }

        suspend fun getIdDetail(id: String): Details {
            try {
                val path = (AppMockup.BASE_URL+AppMockup.DETAIL+id)
                val url = URL(path)
                val urlConnection: HttpURLConnection = url.openConnection() as HttpURLConnection
                try {
                    val input: InputStream = BufferedInputStream(urlConnection.inputStream)
                    Log.d("log", "NETWORK_START_:")
                    Log.d("log", "NETWORK_RESULT_:")
                    val reader = JsonReader(InputStreamReader(input, "UTF-8"))
                    return try {
                        reader.getDetails().apply { Log.d("log", TAG) }
                    } catch (io: IOException) {
                        io.printStackTrace()
                        Details.empty
                    }
                } catch (e: IOException) {
                    Log.e("log", ERROR_TAG)
                    return Details.empty
                } finally {
                    urlConnection.disconnect()
                }
            } catch (e: MalformedURLException) {
                e.printStackTrace()
                return Details.empty
            } catch (e: IOException) {
                e.printStackTrace()
                return Details.empty
            }
        }

        private fun JsonReader.getResults(): Results {
            val recipes: ArrayList<Recipe> = arrayListOf()
            beginObject()
            while (hasNext()) {
                val key = nextName()
                if (key == "result") {
                    beginArray()
                    while (hasNext()) {
                        getRecipe()?.let { recipes.add(it) }
                    }
                    endArray()
                } else {
                    skipValue()
                }
            }
            endObject()
            return Results(recipes.toList())
        }

        private fun JsonReader.getRecipe(): Recipe? {
            beginObject()
            var (id, name, image) = Recipe.empty
            while (hasNext()) {
                when (nextName()) {
                    Recipe.ID -> id = try {
                        nextLong()
                    } catch (e: IOException) {
                        -1L
                    }

                    Recipe.NAME -> name = try {
                        nextString()
                    } catch (e: IllegalStateException) {
                        String()
                    }

                    Recipe.IMAGE -> image = try {
                        nextString()
                    } catch (e: IllegalStateException) {
                        String()
                    }

                    else -> skipValue()
                }
            }
            endObject()
            return if (id != -1L) Recipe(id, name, image) else null
        }

        private fun JsonReader.getDetails(): Details {
            var (name, image, text, ingredients, dish, cockType, prepTime, cockTime, rations, longitude, latitude) = Details.empty
            beginObject()
            while (hasNext()) {
                when (nextName()) {
                    Details.NAME -> name = try {
                        nextString()
                    } catch (e: IllegalStateException) {
                        String()
                    }

                    Details.IMAGE -> image = try {
                        nextString()
                    } catch (e: IllegalStateException) {
                        String()
                    }

                    Details.TEXT -> text = try {
                        nextString()
                    } catch (e: IllegalStateException) {
                        String()
                    }

                    Details.INGREDIENTS -> ingredients = getIngredients()
                    Details.DISH -> dish = try {
                        nextString()
                    } catch (e: IllegalStateException) {
                        String()
                    }

                    Details.COCK_TYPE -> cockType = try {
                        nextString()
                    } catch (e: IllegalStateException) {
                        String()
                    }

                    Details.PREP_TIME -> prepTime = try {
                        nextString()
                    } catch (e: IllegalStateException) {
                        String()
                    }

                    Details.COCK_TIME -> cockTime = try {
                        nextString()
                    } catch (e: IllegalStateException) {
                        String()
                    }

                    Details.RATIONS -> rations = try {
                        nextString()
                    } catch (e: IllegalStateException) {
                        String()
                    }

                    Details.LONGITUDE -> longitude = try {
                        nextString()
                    } catch (e: IllegalStateException) {
                        String()
                    }

                    Details.LATITUDE -> latitude = try {
                        nextString()
                    } catch (e: IllegalStateException) {
                        String()
                    }

                    else -> skipValue()
                }
            }
            endObject()
            return Details(
                name,
                image,
                text,
                ingredients,
                dish,
                cockType,
                prepTime,
                cockTime,
                rations,
                longitude,
                latitude
            )
        }

        private fun JsonReader.getIngredients(): List<String> {
            val list = arrayListOf<String>()
            beginArray()
            while (hasNext()) {
                try {
                    nextString()
                } catch (e: IllegalStateException) {
                    String()
                }.takeIf { it.isNotBlank() }?.let { list.add(it) }
            }
            endArray()
            return list
        }
    }
}


