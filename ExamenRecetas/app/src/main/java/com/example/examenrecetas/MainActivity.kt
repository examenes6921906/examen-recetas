package com.example.examenrecetas

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import com.example.examenrecetas.screens.Main
import com.example.examenrecetas.ui.theme.ExamenRecetasTheme
import android.content.Intent
import android.net.Uri

class MainActivity : ComponentActivity() {
    private val viewModel: RecipesViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ExamenRecetasTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Main(modifier = Modifier.fillMaxSize(), viewModel= viewModel) { lat, lon, q ->
                        onMapClick(lat, lon, q)
                    }

                }
            }
        }
    }

    @SuppressLint("QueryPermissionsNeeded")
    private fun onMapClick(lat: String, lon: String, q: String) {
        if(lat.isNotBlank() && lon.isNotBlank()) {
            val uri = Uri.parse("geo:$lat,$lon?z=8&q=$q")
            val map = Intent(Intent.ACTION_VIEW, uri)
            map.setPackage("com.google.android.apps.maps");
            if (map.resolveActivity(packageManager) != null) {
                startActivity(map);
            }
        }
    }

    companion object {
        const val RECIPES_VIEW = "recipes"
        const val DETAIL_VIEW = "detail"
    }

}
