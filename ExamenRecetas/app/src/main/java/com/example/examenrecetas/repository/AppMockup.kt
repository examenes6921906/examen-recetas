package com.example.examenrecetas.repository

class AppMockup {
    companion object {
        /**
         * Get From Firebase RemoteConfig
         */
        const val BASE_URL: String = "https://demo2900457.mockable.io"
        const val BASE_IMAGE_URL: String = "https://www.comedera.com/wp-content/uploads/"

        const val RECIPES: String = "/recipes"
        const val DETAIL: String = "/detail/" //+id
    }
}