package com.example.examenrecetas.screens

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.example.examenrecetas.RecipesViewModel
import com.example.examenrecetas.model.Details
import com.example.examenrecetas.model.Recipe
import com.example.examenrecetas.model.Routes

import com.example.examenrecetas.repository.AppMockup

@Composable
fun RvRecipe(modifier: Modifier = Modifier, viewModel: RecipesViewModel = RecipesViewModel(), onclick: (Recipe) -> Unit = {}) {
    val recipes by viewModel.respose.collectAsState()
    Column {
        Text(text = "Recetas de cocina",
            modifier = Modifier.align(Alignment.CenterHorizontally),
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold,
            color = Color.Black)
        LazyColumn(verticalArrangement = Arrangement.spacedBy(8.dp),
            modifier = Modifier
                .background(Color.White)
                .padding(16.dp)) {
            items(recipes.results){ recipes ->
                ItemRecipe(recipe = recipes){
                    onclick(recipes)
                }
            }
        }
    }
}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun ItemRecipe(recipe: Recipe,onItemSelected:(Recipe)-> Unit){
    Card(border = BorderStroke(2.dp, Color.Black),
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onItemSelected(recipe) }) {
        Column {
            Text(
                text = recipe.name,
                modifier = Modifier.align(Alignment.CenterHorizontally),
                fontSize = 18.sp,
                color = Color.Black
            )
            GlideImage(
                model = AppMockup.BASE_IMAGE_URL+ recipe.image,
                contentDescription = recipe.name,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 4.dp)
                    .background(Color.White)
                    .aspectRatio(4f / 3f, false))
        }
    }
}

fun getDetail(): Details{
    return Details("receta",
            "receta",
            "dasdasdasldjalsdjalksdjalsdjalsdjalksj",
            listOf("das","asd","asd"),
            "asdasd",
            "3",
            "39",
            "40",
        "2",
        "3.233223",
        "2.3234242")
}
