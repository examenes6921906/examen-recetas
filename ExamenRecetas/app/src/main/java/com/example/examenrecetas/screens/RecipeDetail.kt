package com.example.examenrecetas.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccessTime
import androidx.compose.material.icons.filled.Fastfood
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.RadioButtonChecked
import androidx.compose.material3.Card
import androidx.compose.material3.Divider
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.example.examenrecetas.RecipesViewModel
import com.example.examenrecetas.model.Details
import com.example.examenrecetas.model.Recipe
import com.example.examenrecetas.model.Routes
import com.example.examenrecetas.repository.AppMockup
import org.w3c.dom.Text

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun RecipeDetail(modifier: Modifier = Modifier,
                 viewModel: RecipesViewModel = RecipesViewModel(),
                 onMapClick: (String, String, String) -> Unit = { _,_,_ -> }) {

    val detail by viewModel.detail.collectAsState()

    LazyColumn(
        verticalArrangement = Arrangement.spacedBy(8.dp),
        modifier = Modifier
            .background(Color.White)
            .padding(16.dp)
    ) {
        item {
            Text(
                text = "Recetas de cocina",
                fontSize = 24.sp,
                fontWeight = FontWeight.Bold,
                color = Color.Black
            )
        }
        item{
            Column {
                Text(text = detail.name,
                    modifier = Modifier.align(Alignment.CenterHorizontally),
                    fontSize = 24.sp,
                    fontWeight = FontWeight.Bold,
                    color = Color.Black)
                GlideImage(
                    model = AppMockup.BASE_IMAGE_URL+ detail.image,
                    contentDescription = getDetail().name,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 4.dp, vertical = 4.dp)
                        .background(Color.White)
                        .aspectRatio(4f / 3f, false))
                Text(text = detail.text,
                    modifier = Modifier
                        .align(Alignment.CenterHorizontally)
                        .padding(vertical = 4.dp),
                    fontSize = 12.sp,
                    color = Color.Black)
                Text(text = "Ingredientes",
                    modifier = Modifier
                        .align(Alignment.CenterHorizontally)
                        .padding(bottom = 8.dp),
                    fontSize = 12.sp,
                    color = Color.Black)
                detail.ingredients.forEach {
                    Card (Modifier.fillMaxWidth()){
                        Row {
                            Image(imageVector = Icons.Filled.RadioButtonChecked,
                                contentDescription = "")
                            Text(text = it,
                                modifier = Modifier.padding(start = 8.dp),
                                fontSize = 12.sp,
                                color = Color.Black)   
                        }
                    }
                }
                Row (Modifier.fillMaxWidth()){
                    Image(imageVector = Icons.Filled.AccessTime,
                        contentDescription = "")
                    Text(text = "Tiempo de cocción ${detail.cockTime} minutos",
                        modifier = Modifier.padding(start = 8.dp),
                        fontSize = 12.sp,
                        color = Color.Black)
                }
                MyDivider()
                    Row (Modifier.fillMaxWidth().align(Alignment.CenterHorizontally)){
                        Image(imageVector = Icons.Filled.Fastfood,
                            contentDescription = "")
                        Text(text = "Tipo de cocina ${detail.cockType}",
                            modifier = Modifier.padding(start = 8.dp),
                            fontSize = 12.sp,
                            color = Color.Black)
                    }

                MyDivider()
                Row(Modifier.fillMaxWidth()) {
                    Image(imageVector = Icons.Filled.Person,
                        contentDescription = "")
                    Text(text = "Porciones ${detail.rations} ",
                        modifier = Modifier.padding(start = 8.dp),
                        fontSize = 12.sp,
                        color = Color.Black)
                }
                MyDivider()
                TextButton(onClick = { onMapClick(detail.latitude, detail.longitude, detail.name) }) {
                    Column (Modifier.fillMaxWidth()){
                        Text(
                            text = "Ver Mapa", modifier = Modifier.align(Alignment.CenterHorizontally),
                            fontSize = 14.sp,
                            fontWeight = FontWeight.Bold,
                            color = Color.Blue
                        )
                    }
                }


        }
    }
}
}

@Composable
fun MyDivider(){
    Divider(Modifier.fillMaxWidth(), color = Color.LightGray)
}