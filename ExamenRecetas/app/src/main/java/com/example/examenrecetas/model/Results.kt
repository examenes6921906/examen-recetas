package com.example.examenrecetas.model

data class Results(val results: List<Recipe>) {

    companion object{
        val empty: Results get() { return Results(listOf()) }
    }
}
