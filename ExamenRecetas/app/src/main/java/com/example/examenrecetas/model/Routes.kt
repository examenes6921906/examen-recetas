package com.example.examenrecetas.model

sealed class Routes(val route: String) {

    object RecipeList: Routes("rvRecipe?listRecipe={listRecipe}"){
        fun createRoute(listRecipe:List<Recipe>)= "rvRecipe?detail=$listRecipe"
    }
    object Detail: Routes("recipeDetail?detail={detail}"){
        fun createRoute(detail:Details)= "recipeDetail?detail=$detail"
    }
}