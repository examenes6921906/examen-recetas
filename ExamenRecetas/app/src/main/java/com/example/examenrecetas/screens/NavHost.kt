package com.example.examenrecetas.screens
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.examenrecetas.MainActivity
import com.example.examenrecetas.RecipesViewModel
import com.example.examenrecetas.model.Routes.RecipeList


@Composable
fun Main(modifier: Modifier, viewModel: RecipesViewModel, onMapClick: (String, String, String) -> Unit = {_,_,_ ->}) {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = MainActivity.RECIPES_VIEW) {
        composable(MainActivity.RECIPES_VIEW) {
            RvRecipe(modifier, viewModel) { recipe ->
                viewModel.getDetails(recipe.id.toString())
                navController.navigate(MainActivity.DETAIL_VIEW)
            }
        }
        composable(MainActivity.DETAIL_VIEW) { RecipeDetail(modifier, viewModel, onMapClick) }
    }
}
