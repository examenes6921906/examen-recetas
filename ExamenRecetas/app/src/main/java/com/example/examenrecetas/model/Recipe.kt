package com.example.examenrecetas.model

data class Recipe(
    val id: Long,
    val name: String,
    val image: String
) {
    companion object {
        const val ID = "id"
        const val NAME = "nombre"
        const val IMAGE = "image"

        val empty: Recipe
            get() {
                return Recipe(-1, String(), String())
            }
    }
}
